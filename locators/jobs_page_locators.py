JOBS_IFRAME = "icims_content_iframe"
JOBS_SEARCH_FIELD = "jsb_f_keywords_i"
JOBS_LOCATION_DROPDOWN = "jsb_f_location_s"
BUTTONS = {"Search": "#jsb_form_submit_i", "Apply for this job online": "a[title='Apply for this job online']",
           "Create with Online Form": "a[title='Create with Online Form']"}
JOBS_RESULTS = ".iCIMS_JobsTableField_1 a"
NOTIFICATION = "div.iCIMS_MainWrapper div.iCIMS_ErrorMessage"
