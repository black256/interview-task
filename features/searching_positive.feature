Feature: Logged out customer is able to search for a job

  Scenario: Search for a job matching given criteria
     Given Customer is on "https://jobs-akamai.icims.com/jobs/intro"
      When Customer specifies job title : "Test"
       And Location : "PL-Krakow"
       And clicks on "Search"
      Then Any job offers are found

  Scenario:	Select one of the job offers in PL-Krakow
     Given Customer found matching job offers
      When Customer click on any of the offers
       And on job offer details, customer click on "Apply for this job online"
       And clicks on "Create with Online Form"
      Then Customer is on personal details form
