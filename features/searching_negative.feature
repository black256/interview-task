Feature: Customer is notified when no offers match given criteria

  Scenario: Search for a job matching given criteria
     Given Customer is on "https://jobs-akamai.icims.com/jobs/intro"
      When Customer specifies job title : "XXX"
       And clicks on "Search"
      Then Notification about no offers found is displayed
