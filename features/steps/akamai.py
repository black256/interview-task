from behave import *


@given('Customer is on "{url}')
def step_impl(context, url):
    context.jobs_page.go_to(url)


@when('Customer specifies job title : "{title}"')
def step_impl(context, title):
    context.jobs_page.type_job_title(title)


@when('Location : "{location}"')
def step_impl(context, location):
    context.jobs_page.specify_location(location)


@when('clicks on "{button}"')
def step_impl(context, button):
    context.jobs_page.click_button(button)


@then('Any job offers are found')
@given('Customer found matching job offers')
def step_impl(context):
    context.jobs_page.are_job_offers_found()


@when('Customer click on any of the offers')
def step_impl(context):
    context.jobs_page.select_random_offer()


@when('on job offer details, customer click on "{button}"')
def step_impl(context, button):
    context.jobs_page.click_button(button)


@then('Customer is on personal details form')
def step_impl(context):
    context.form_page.is_form_present()


@then('Notification about no offers found is displayed')
def step_impl(context):
    context.jobs_page.is_notifications_displayed()
