from selenium import webdriver
from objects.jobs_page_object import JobsPageObject
from objects.form_page_object import FormPageObject


def before_feature(context, feature):
    context.browser = getattr(webdriver, context.config.userdata.get("browser", "Chrome"))()
    context.jobs_page = JobsPageObject(context.browser)
    context.form_page = FormPageObject(context.browser)


def after_feature(context, feature):
    context.browser.quit()
