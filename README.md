# Akamai Job portal tests

### What is required to run the tests?

* Python 2.7
    * For Windows please install [ActivePython x86](http://www.activestate.com/activepython/downloads) 
    * For OS X preferably install Python using [Homebrew](http://brew.sh)
    * Both comes with *pip* installed
* Depending on the browser you want to use for testing, you will need:
    * For Chrome - [chromedriver](https://code.google.com/p/selenium/wiki/ChromeDriver) - added to PATH
    * For Firefox - just install [Firefox](https://www.mozilla.org/en-US/firefox/new/)
    * For headless testing - [PhantomJS](http://phantomjs.org) - added to PATH
* To finally prepare your environment, after cloning the project open the directory in console and run ```pip install -r requirements.txt```, which will install all required Python dependencies

### How to run tests?

In projects root directory run


```bash
behave -D browser=%browser%
```

- `%browser%` - choose from Chrome, Firefox or PhantomJS

### Links

* [behave](http://pythonhosted.org/behave/index.html)
* [Selenium WebDriver](http://www.seleniumhq.org/projects/webdriver/)
