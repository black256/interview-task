import random
from locators.jobs_page_locators import *
from selenium.webdriver.support.ui import Select


class JobsPageObject(object):

    def __init__(self, driver):
        self.browser = driver

    def go_to(self, url):
        self.browser.get(url)

    def type_job_title(self, title):
        self.browser.switch_to_frame(JOBS_IFRAME)
        self.browser.find_element_by_id(JOBS_SEARCH_FIELD).send_keys(title)

    def specify_location(self, location):
        dropdown = Select(self.browser.find_element_by_id(JOBS_LOCATION_DROPDOWN))
        dropdown.select_by_visible_text(location)

    def click_button(self, button):
        self.browser.find_element_by_css_selector(BUTTONS[button]).click()

    def number_of_job_offers(self):
        return len(self.browser.find_elements_by_css_selector(JOBS_RESULTS))

    def are_job_offers_found(self):
        assert self.number_of_job_offers() > 0

    def select_random_offer(self):
        options = random.randint(0, self.number_of_job_offers() - 1)
        self.browser.find_elements_by_css_selector(JOBS_RESULTS)[options].click()

    def is_notifications_displayed(self):
        notification = self.browser.find_element_by_css_selector(NOTIFICATION)
        assert notification.is_displayed()
