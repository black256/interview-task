from locators.form_page_locators import *


class FormPageObject(object):

    def __init__(self, driver):
        self.browser = driver

    def is_form_present(self):
        assert self.header_text() == "Create Profile:"
        assert self.is_table_present()
        assert self.is_first_name_present()
        assert self.is_middle_name_present()
        assert self.is_last_name_present()
        assert self.is_email_present()
        assert self.is_login_present()
        assert self.is_password_present()
        assert self.is_password_confirm_present()
        assert self.is_submit_present()

    def header_text(self):
        return self.browser.find_element_by_css_selector(FORM_HEADER).text

    def is_table_present(self):
        return self.browser.find_element_by_css_selector(FORM_PROFILE_TABLE).is_displayed()

    def is_first_name_present(self):
        return self.browser.find_element_by_id(FORM_FIRST_NAME).is_displayed()

    def is_middle_name_present(self):
        return self.browser.find_element_by_id(FORM_MIDDLE_NAME).is_displayed()

    def is_last_name_present(self):
        return self.browser.find_element_by_id(FORM_LAST_NAME).is_displayed()

    def is_email_present(self):
        return self.browser.find_element_by_id(FORM_EMAIL).is_displayed()

    def is_login_present(self):
        return self.browser.find_element_by_id(FORM_LOGIN).is_displayed()

    def is_password_present(self):
        return self.browser.find_element_by_id(FORM_PASSWORD).is_displayed()

    def is_password_confirm_present(self):
        return self.browser.find_element_by_id(FORM_PASSWORD_CONFIRM).is_displayed()

    def is_submit_present(self):
        return self.browser.find_element_by_id(FORM_SUBMIT_BUTTON).is_displayed()
